using Godot;
using System;

public partial class PlayerController : CharacterBody3D
{
    private const float MaxScreenShake = 0.4f;
    private int _speed = 10;
    private int _horizontalAcceleration = 6;
    private int _airAcceleration = 6;
    private int _normalAcceleration = 6;
    private float _gravity = ProjectSettings.GetSetting("physics/3d/default_gravity").AsSingle();
    private float _jumpVelocity = 5.5f;
    private bool _fullContact = false;
    private float _mouseSensitivity = 0.03f;
    private Node3D _head;
    private Camera3D _camera;

    public override void _Ready()
    {
        _head = GetNode<Node3D>("Head");
        Input.MouseMode = Input.MouseModeEnum.Captured;
    }

    public override void _PhysicsProcess(double delta)
    {
        Vector3 velocity = Velocity;

        // Add the gravity.
        if (!IsOnFloor())
            velocity.y -= _gravity * (float) delta;

        // Handle Jump.
        if (Input.IsActionJustPressed("ui_accept") && IsOnFloor())
            velocity.y = _jumpVelocity;

        // Get the input direction and handle the movement/deceleration.
        // As good practice, you should replace UI actions with custom gameplay actions.
        Vector2 inputDir = Input.GetVector("ui_left", "ui_right", "ui_up", "ui_down");
        Vector3 direction = (Transform.basis * new Vector3(inputDir.x, 0, inputDir.y)).Normalized();
        if (direction != Vector3.Zero)
        {
            velocity.x = direction.x * _speed;
            velocity.z = direction.z * _speed;
        }
        else
        {
            velocity.x = Mathf.MoveToward(Velocity.x, 0, _speed);
            velocity.z = Mathf.MoveToward(Velocity.z, 0, _speed);
        }

        Velocity = velocity;
        MoveAndSlide();
    }

    public override void _Input(InputEvent @event)
    {
        if (@event is InputEventMouseMotion inputMouseMotion)
        {
            RotateY(Mathf.DegToRad(-inputMouseMotion.Relative.x * _mouseSensitivity));
            _head.RotateX(Mathf.DegToRad(-inputMouseMotion.Relative.y * _mouseSensitivity));
            Vector3 rotationDegrees = _head.RotationDegrees;
            rotationDegrees.x = Mathf.Clamp(rotationDegrees.x, -80f, 80f);
            _head.RotationDegrees = rotationDegrees;
        }
    }
}